package com.adapters.extensions

import androidx.fragment.app.Fragment

fun Fragment.toast(msg: String) {
    requireContext().toast(msg)
}