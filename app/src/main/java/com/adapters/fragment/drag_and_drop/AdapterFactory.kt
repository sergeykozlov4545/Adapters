package com.adapters.fragment.drag_and_drop

import android.graphics.Color
import com.adapterdsl.adapter
import com.adapterdsl.singleTypeHolder
import com.adapters.databinding.ItemTextBinding
import com.adapters.entity.Item
import com.adapters.extensions.inflater
import com.adapters.fragment.Mocks
import java.util.*

internal val adapter = adapter(startItems = Mocks.items) {
    dragEndDrop(enabled = true) {
        itemDrag { adapter, viewHolder, position ->
            (0 until adapter.itemCount)
                .filter { itemIndex -> itemIndex != position }
                .forEach { itemIndex -> adapter.notifyItemChanged(itemIndex) }
            viewHolder.itemView.setBackgroundColor(Color.LTGRAY)
        }
        itemMove { adapter, from, to ->
            Collections.swap(adapter.items, from, to)
            adapter.notifyItemMoved(from, to)
            return@itemMove true
        }
        itemDrop { adapter, _, _ -> adapter.notifyDataSetChanged() }
    }

    singleTypeHolder<Item, ItemTextBinding> {
        viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
        bindViewHolder { item, _ ->
            binding.apply {
                tvText.text = "${item.value}, ${item.code}"
                root.setBackgroundColor(if (dragHelper.isDragging()) Color.RED else 0)
            }
        }
    }
}