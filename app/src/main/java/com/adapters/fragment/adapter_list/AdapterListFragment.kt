package com.adapters.fragment.adapter_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapters.R
import com.adapters.databinding.FragmentAdapterListBinding
import com.adapters.fragment.common.BaseFragment

class AdapterListFragment : BaseFragment<FragmentAdapterListBinding>() {

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentAdapterListBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            bSingleHolder.setOnClickListener { runNavAction(R.id.action_single_holder) }
            bSeveralHolder.setOnClickListener { runNavAction(R.id.action_several_holder) }
            bIsFoMe.setOnClickListener { runNavAction(R.id.action_custom_is_for_me) }
        }
    }
}