package com.adapters.fragment.adapter_list.single_holder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapterdsl.adapter
import com.adapterdsl.singleTypeHolder
import com.adapters.databinding.FragmentItemsBinding
import com.adapters.databinding.ItemTextBinding
import com.adapters.entity.Animal
import com.adapters.extensions.inflater
import com.adapters.extensions.toast
import com.adapters.fragment.Mocks
import com.adapters.fragment.common.BaseFragment

class SingleHolderFragment : BaseFragment<FragmentItemsBinding>() {

    private val adapter =
        adapter(startItems = Mocks.animals) {
            diffCallback {
                itemsComparator { animalOne, animalTwo -> animalOne.name == animalTwo.name }
                contentsComparator { animalOne, animalTwo -> animalOne == animalTwo }
            }
            singleTypeHolder<Animal, ItemTextBinding> {
                viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
                bindHolder { animal, _ ->
                    tvText.text = "Animal holder: ${animal.name}"
                    root.setOnClickListener { toast("${animal.name} clicked") }
                }
            }
        }

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentItemsBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            rvItems.adapter = adapter
        }
    }
}