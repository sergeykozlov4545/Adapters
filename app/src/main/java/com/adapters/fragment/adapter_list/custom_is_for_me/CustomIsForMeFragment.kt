package com.adapters.fragment.adapter_list.custom_is_for_me

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapters.databinding.FragmentItemsBinding
import com.adapters.extensions.toast
import com.adapters.fragment.common.BaseFragment

class CustomIsForMeFragment : BaseFragment<FragmentItemsBinding>() {

    private val adapter = adapterFactory { msg -> toast(msg) }

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentItemsBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            rvItems.adapter = adapter
        }
    }
}