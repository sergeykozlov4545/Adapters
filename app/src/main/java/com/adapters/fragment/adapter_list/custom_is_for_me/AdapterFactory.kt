package com.adapters.fragment.adapter_list.custom_is_for_me

import com.adapterdsl.Adapter
import com.adapterdsl.adapter
import com.adapterdsl.singleTypeHolder
import com.adapters.databinding.ItemTextBinding
import com.adapters.entity.Item
import com.adapters.entity.ItemCode
import com.adapters.extensions.inflater
import com.adapters.fragment.Mocks

internal fun adapterFactory(onClick: (msg: String) -> Unit) =
    adapter(startItems = Mocks.items) {
        itemHolder(ItemCode.TypeOne, "first type clicked", onClick)
        itemHolder(ItemCode.TypeTwo, "second type clicked", onClick)
        itemHolder(ItemCode.TypeThree, "third type clicked", onClick)
    }

private fun Adapter<Item>.itemHolder(
    code: ItemCode,
    msg: String,
    onClick: (msg: String) -> Unit
) =
    singleTypeHolder<Item, ItemTextBinding> {
        isForMe { item -> item.code == code }
        viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
        bindHolder { value, _ ->
            tvText.text = "${value.value}, ${value.code}"
            root.setOnClickListener { onClick(msg) }
        }
    }
