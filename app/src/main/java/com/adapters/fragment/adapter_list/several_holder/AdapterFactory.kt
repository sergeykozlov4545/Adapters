package com.adapters.fragment.adapter_list.several_holder

import com.adapterdsl.Adapter
import com.adapterdsl.adapter
import com.adapterdsl.holder
import com.adapters.databinding.ItemTextBinding
import com.adapters.entity.Animal
import com.adapters.entity.Cat
import com.adapters.entity.Dog
import com.adapters.entity.Rabbit
import com.adapters.extensions.inflater
import com.adapters.fragment.Mocks

internal fun adapterFactory(onClick: (Animal) -> Unit) =
    adapter(startItems = Mocks.animals) {
        diffCallback {
            itemsComparator { animalOne, animalTwo -> animalOne.name == animalTwo.name }
            contentsComparator { animalOne, animalTwo -> animalOne == animalTwo }
        }

        catHolder(onClick)
        dogHolder(onClick)
        rabbitHolder(onClick)
    }

private fun Adapter<Animal>.catHolder(onClick: (Animal) -> Unit) =
    holder<Animal, Cat, ItemTextBinding> {
        viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
        bindHolder { value, _ ->
            tvText.text = "Cat holder: ${value.name}, ${value.age}"
            root.setOnClickListener { onClick(value) }
        }
    }

private fun Adapter<Animal>.dogHolder(onClick: (Animal) -> Unit) =
    holder<Animal, Dog, ItemTextBinding> {
        viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
        bindHolder { value, _ ->
            tvText.text = "Dog holder: ${value.name}"
            root.setOnClickListener { onClick(value) }
        }
    }

private fun Adapter<Animal>.rabbitHolder(onClick: (Animal) -> Unit) =
    holder<Animal, Rabbit, ItemTextBinding> {
        viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
        bindHolder { value, _ ->
            tvText.text = "Rabbit holder: ${value.name}"
            root.setOnClickListener { onClick(value) }
        }
    }