package com.adapters.fragment.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<T : ViewBinding> : Fragment() {

    protected lateinit var binding: T

    protected abstract fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?): T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        onCreateViewBinding(inflater, container)
            .also { binding = it }
            .root

    protected fun configureView(init: T.() -> Unit) {
        init(binding)
    }

    protected fun runNavAction(id: Int) {
        findNavController().navigate(id)
    }
}