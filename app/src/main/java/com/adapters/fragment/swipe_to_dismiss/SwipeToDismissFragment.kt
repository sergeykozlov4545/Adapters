package com.adapters.fragment.swipe_to_dismiss

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapters.databinding.FragmentItemsBinding
import com.adapters.fragment.common.BaseFragment

class SwipeToDismissFragment : BaseFragment<FragmentItemsBinding>() {

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentItemsBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            rvItems.adapter = adapter
        }
    }
}