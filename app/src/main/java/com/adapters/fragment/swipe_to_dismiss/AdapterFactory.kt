package com.adapters.fragment.swipe_to_dismiss

import android.graphics.Color
import com.adapterdsl.adapter
import com.adapterdsl.singleTypeHolder
import com.adapters.databinding.ItemTextBinding
import com.adapters.entity.Item
import com.adapters.extensions.inflater
import com.adapters.fragment.Mocks

internal val adapter = adapter(startItems = Mocks.items) {
    swipeToDismiss(enabled = true)

    singleTypeHolder<Item, ItemTextBinding> {
        viewBinding { parent -> ItemTextBinding.inflate(parent.inflater, parent, false) }
        bindViewHolder { item, _ ->
            binding.apply {
                tvText.text = "${item.value}, ${item.code}"
                root.setBackgroundColor(if (dragHelper.isDragging()) Color.RED else 0)
            }
        }
    }
}