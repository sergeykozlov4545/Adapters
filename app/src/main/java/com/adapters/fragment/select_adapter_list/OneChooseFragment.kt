package com.adapters.fragment.select_adapter_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapterdsl.chooseadapter.oneChooseAdapter
import com.adapters.databinding.FragmentItemsBinding
import com.adapters.extensions.toast
import com.adapters.fragment.Mocks
import com.adapters.fragment.common.BaseFragment

class OneChooseFragment : BaseFragment<FragmentItemsBinding>() {

    private val adapter =
        oneChooseAdapter(startItems = Mocks.items) {
            title { item -> item.value }
            subtitle { item -> item.code.toString() }
            onClick { item, _ -> toast("item ${item.value} clicked") }
        }

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentItemsBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            rvItems.adapter = adapter
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(BUNDLE_SELECTED_INDEX, adapter.selectedIndex)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val savedSelectedIndex = savedInstanceState?.getInt(BUNDLE_SELECTED_INDEX)
        savedSelectedIndex?.let { adapter.selectedIndex = it }
    }

    companion object {
        private const val BUNDLE_SELECTED_INDEX = "selected_index"
    }
}