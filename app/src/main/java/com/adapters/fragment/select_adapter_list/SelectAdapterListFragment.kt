package com.adapters.fragment.select_adapter_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapters.R
import com.adapters.databinding.FragmentSelectAdapterListBinding
import com.adapters.fragment.common.BaseFragment

class SelectAdapterListFragment : BaseFragment<FragmentSelectAdapterListBinding>() {

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentSelectAdapterListBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            bOneSelect.setOnClickListener { runNavAction(R.id.action_one_select) }
            bMultiSelect.setOnClickListener { runNavAction(R.id.action_multi_select) }
        }
    }
}