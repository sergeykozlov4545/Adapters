package com.adapters.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adapters.R
import com.adapters.databinding.FragmentMainButtonsBinding
import com.adapters.fragment.common.BaseFragment

class MainButtonsFragment : BaseFragment<FragmentMainButtonsBinding>() {

    override fun onCreateViewBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentMainButtonsBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView {
            bAdapterList.setOnClickListener { runNavAction(R.id.action_base_operations) }
            bSelectAdapterList.setOnClickListener { runNavAction(R.id.action_select_adapter) }
            bDragAndDrop.setOnClickListener { runNavAction(R.id.action_drag_end_drop) }
            bSwipeToDismiss.setOnClickListener { runNavAction(R.id.action_swipe_to_dismiss) }
        }
    }
}