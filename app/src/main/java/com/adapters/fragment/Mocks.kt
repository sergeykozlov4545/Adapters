package com.adapters.fragment

import com.adapters.entity.*

object Mocks {
    val animals = listOf(
        Cat("Cat 1", 11),
        Dog("Dog 1"),
        Rabbit("Rabbit 1"),
        Cat("Cat 2", 22),
        Dog("Dog 2"),
        Rabbit("Rabbit 2"),
        Cat("Cat 3", 33),
        Dog("Dog 3"),
        Rabbit("Rabbit 3")
    )

    val items = listOf(
        Item(value = "Item 1", code = ItemCode.TypeOne),
        Item(value = "Item 2", code = ItemCode.TypeTwo),
        Item(value = "Item 3", code = ItemCode.TypeThree),
        Item(value = "Item 4", code = ItemCode.TypeOne),
        Item(value = "Item 5", code = ItemCode.TypeTwo),
        Item(value = "Item 6", code = ItemCode.TypeThree),
        Item(value = "Item 7", code = ItemCode.TypeOne),
        Item(value = "Item 8", code = ItemCode.TypeTwo),
        Item(value = "Item 9", code = ItemCode.TypeThree)
    )
}