package com.adapters.entity

open class Animal(val name: String)

class Cat(name: String, val age: Int) : Animal(name)
class Dog(name: String) : Animal(name)
class Rabbit(name: String) : Animal(name)
