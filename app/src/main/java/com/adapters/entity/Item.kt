package com.adapters.entity

data class Item(
    val value: String,
    val code: ItemCode
)

sealed class ItemCode {
    object TypeOne : ItemCode() {
        override fun toString() = "one"
    }

    object TypeTwo : ItemCode() {
        override fun toString() = "two"
    }

    object TypeThree : ItemCode() {
        override fun toString() = "three"
    }
}

