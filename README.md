#### Описание
Dsl-библиотека для создания RecyclerView.Adapter. Под капотом для отображения ViewHolder используется ViewBinding.

#### Структура репозитория
* app - примеры использования библиотеки.
* lib - библиотека.

#### Примеры использования

##### Базовый dsl
Пример 1. Пример использования библиотеки когда имеется один тип данных.
```
val adapter = adapter(startItems = items) {
    diffCallback {
        itemsComparator { itemOne, itemTwo -> 
            // тут сравнить по id или чему-то другому 
        }
        contentsComparator { itemOne, itemTwo -> 
            // тут сравнить содержимое объектов
        }
    }
    singleTypeHolder<ItemType, ItemBinding> { // ItemType - тип пункта, ItemBinding - это layout вашего пункта
        viewBinding { parent -> ItemBinding.inflate(parent.inflater, parent, false) }
        bindHolder { item, position ->  
            // Настройка Holder'a
        }
    }
}
```
Пример 2. Пример использования библиотеки когда имеется один тип данных, но из-за состояния этих данных могут быть разные Holder'ы.
```
val adapter = adapter(startItems = items) {
    ...
    singleTypeHolder<ItemType, ItemBinding> { // ItemType - тип пункта, ItemBinding - это layout вашего пункта
        isForMe { item -> 
            // тут условие по которому определяется что нужно использовать этот Holder
        }
        ...
    }
    singleTypeHolder<ItemType, ItemBinding> { // ItemType - тип пункта, ItemBinding - это layout вашего пункта
        isForMe { item -> 
            // тут условие по которому определяется что нужно использовать этот Holder
        }
        ...
    }
    singleTypeHolder<ItemType, ItemBinding> { // ItemType - тип пункта, ItemBinding - это layout вашего пункта
        isForMe { item -> 
            // тут условие по которому определяется что нужно использовать этот Holder
        }
        ...
    }
}
```
Пример 3. Пример использования библиотеки когда имеется один общий тип данных и несколько наследников.
```
val adapter = adapter<ParentItemType>(startItems = items) {
    ...
    holder<ParentItemType, ItemType, ItemBinding> { // ParentItemType - родитеский тип, ItemType - тип пункта, ItemBinding - это layout вашего пункта
        ...
        // isForMe тут уже не нужно. Библиотека будет искать по типу пункта.
    }
    holder<ParentItemType, ItemType, ItemBinding> { // ParentItemType - родитеский тип, ItemType - тип пункта, ItemBinding - это layout вашего пункта
        ...
        // isForMe тут уже не нужно. Библиотека будет искать по типу пункта.
    }
    holder<ParentItemType, ItemType, ItemBinding> { // ParentItemType - родитеский тип, ItemType - тип пункта, ItemBinding - это layout вашего пункта
        ...
        // isForMe тут уже не нужно. Библиотека будет искать по типу пункта.
    }
}
```

##### Choose adapter
Одиночный выбор
```
val adapter = oneChooseAdapter(startItems = items) {
    title { item -> 
        // Тут отображение title. Вызов данной функции обязательно.
    }
    subtitle { item -> 
        // Тут отображение subtitle. Вызов данной функции необязательно.
    }
    onClick { item, position -> 
        // Обработка клика если нужно. Вызов данной функции необязательно.
    }
}
```
Множественный выбор
```
val adapter = multiChooseAdapter(startItems = items) {
    title { item -> 
        // Тут отображение title. Вызов данной функции обязательно.
    }
    subtitle { item -> 
        // Тут отображение subtitle. Вызов данной функции необязательно.
    }
    onClick { item, position -> 
        // Обработка клика если нужно. Вызов данной функции необязательно.
    }
}
```

##### DragEndDrop/SwipeToDismiss
Компактная версия. Значения callback'ов по умолчанию.
```
val adapter = adapter(startItems = items) {
    dragEndDrop(enabled = true) 
    swipeToDismiss(enabled = true)
    ...
}
```
Полная версия
```
val adapter = adapter(startItems = items) {
    dragEndDrop(enabled = true) {
        itemDrag { adapter, viewHolder, position -> 
            // Тут обработка начала перетаскивания
        }
        itemMove { adapter, from, to ->
            // Тут обработка перетаскивания
        }
        itemDrop { adapter, viewHolder, position -> 
            // Тут обработка после окончания перетаскивания
        }
    }
    swipeToDismiss(enabled = true) {
        itemStartSwipe { viewHolder, position ->
            // Тут обработка начала смахивания
        }
        itemDismiss { adapter, position -> 
            // Тут обработка когда элемент был удален из списка
        }
        itemEndSwipe { viewHolder, position -> 
            // Тут обработка после смахивания
        }
    }
    ...
}
```
