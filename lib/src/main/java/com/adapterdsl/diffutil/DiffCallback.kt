package com.adapterdsl.diffutil

import androidx.recyclerview.widget.AdapterListUpdateCallback
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import com.adapterdsl.Adapter
import com.adapterdsl.DslDiffUtil

/**
 * Интерфейс для dsl-конфигурирования DiffCallback'a
 * */
interface DiffCallbackConfiguration<T> {
    /**
     * Задает comparator для функции ItemCallback.areItemsTheSame
     *
     * @see DiffUtil.ItemCallback.areItemsTheSame
     * */
    fun itemsComparator(comparator: (oldItem: T, newItem: T) -> Boolean)

    /**
     * Задает comparator для функции ItemCallback.areContentsTheSame
     *
     * @see DiffUtil.ItemCallback.areContentsTheSame
     * */
    fun contentsComparator(comparator: (oldItem: T, newItem: T) -> Boolean)

    /**
     * Создает AsyncListDiffer
     *
     * @see AsyncListDiffer
     * */
    fun createDiffer(adapter: Adapter<T>): AsyncListDiffer<T>
}

/**
 * ItemCallback для DiffUtil.
 *
 * @see DiffUtil.ItemCallback
 * */
@DslDiffUtil
class DiffCallback<T> : DiffUtil.ItemCallback<T>(), DiffCallbackConfiguration<T> {

    private var itemsComparator: ((oldItem: T, newItem: T) -> Boolean) = { _, _ -> false }
    private var contentsComparator: ((oldItem: T, newItem: T) -> Boolean) =
        { oldItem: T, newItem: T -> oldItem == newItem }

    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
        itemsComparator(oldItem, newItem)

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
        contentsComparator(oldItem, newItem)

    override fun getChangePayload(oldItem: T, newItem: T) = Any()

    override fun itemsComparator(comparator: (oldItem: T, newItem: T) -> Boolean) {
        this.itemsComparator = comparator
    }

    override fun contentsComparator(comparator: (oldItem: T, newItem: T) -> Boolean) {
        this.contentsComparator = comparator
    }

    override fun createDiffer(adapter: Adapter<T>) =
        AsyncListDiffer(
            AdapterListUpdateCallback(adapter),
            AsyncDifferConfig.Builder(this).build()
        )
}
