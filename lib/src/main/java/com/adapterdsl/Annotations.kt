package com.adapterdsl

/**
 * DslMarker для Adapter
 *
 * @see DslMarker
 * @see Adapter
 * */
@DslMarker
annotation class DslAdapter

/**
 * DslMarker для Holder
 *
 * @see DslMarker
 * @see com.adapterdsl.holdersmanager.Holder
 * */
@DslMarker
annotation class DslHolder

/**
 * DslMarker для DiffCallback
 *
 * @see DslMarker
 * @see com.adapterdsl.diffutil.DiffCallback
 * */
@DslMarker
annotation class DslDiffUtil

/**
 * DslMarker для DragHelper
 *
 * @see DslMarker
 * @see com.adapterdsl.itemtouch.DragHelper
 * */
@DslMarker
annotation class DslDragHelper

/**
 * DslMarker для SwipeHelper
 *
 * @see DslMarker
 * @see com.adapterdsl.itemtouch.SwipeHelper
 * */
@DslMarker
annotation class DslSwipeHelper