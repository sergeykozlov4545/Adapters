package com.adapterdsl.holdersmanager

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adapterdsl.itemtouch.DragHelper
import com.adapterdsl.itemtouch.SwipeHelper

/**
 * Manager для управления Holder'ми.
 *
 * @see Holder
 * */
internal class HoldersManager<T> {
    private var maxViewType: Int = -1
    private val holders = mutableMapOf<Int, Holder<T>>()

    /**
     * Оператор для добавления Holder'a в Manager
     * */
    operator fun plusAssign(holder: Holder<T>) {
        holders[++maxViewType] = holder
    }

    /**
     * Создать ViewHolder зная его viewType
     * */
    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder = holders[viewType] ?: throw IllegalArgumentException("not found holder")
        return holder.onCreateViewHolder(parent)
    }

    /**
     * Получить viewType Holder'a зная позицию в списке
     * */
    fun getItemViewType(items: List<T>, position: Int): Int {
        val value = items[position] ?: return 0
        holders.entries.forEach { (viewType, holder) ->
            if (holder.isForMe(value)) {
                return viewType
            }
        }
        return 0
    }

    /**
     * Обновить состояние Holder'a
     * */
    fun onBindViewHolder(
        ViewHolder: RecyclerView.ViewHolder,
        dragHelper: DragHelper<T>,
        swipeHelper: SwipeHelper<T>,
        items: List<T>,
        position: Int
    ) {
        val holder = holders[ViewHolder.itemViewType] ?: return
        val value = items[position] ?: return
        holder.onBindViewHolder(ViewHolder, dragHelper, swipeHelper, value, position)
    }
}