package com.adapterdsl.holdersmanager

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.adapterdsl.DslHolder
import com.adapterdsl.ViewBindingHolder
import com.adapterdsl.itemtouch.DragHelper
import com.adapterdsl.itemtouch.SwipeHelper
import kotlinx.coroutines.CoroutineScope

/**
 * Интерфейс Holder'a.
 * */
interface Holder<T> {
    /**
     * Предназначена ли данный холдер для отображения value.
     * Используется для поиска холдера и определения его ViewType
     * */
    fun isForMe(value: T): Boolean

    /**
     * Создать ViewHolder
     * */
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    /**
     * Обновить состояние ViewHolder'a
     * @param holder сам холдер
     * @param dragHelper helper для перетаскивания пунктов списка
     * @param swipeHelper helper для смахивания пунктов списка
     * @param value значение
     * @param position позиция в списке
     *
     * @see DragHelper
     * @see SwipeHelper
     * */
    fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        dragHelper: DragHelper<T>,
        swipeHelper: SwipeHelper<T>,
        value: T,
        position: Int
    )
}

/**
 * Интерфейс для dsl-конфигурирования Holder'a
 * */
interface HolderConfiguration<T, S : T, VB : ViewBinding> {
    /**
     * Устанавливает callback для функции Holder.isForMe
     *
     * @see Holder
     * */
    fun isForMe(callback: (value: T) -> Boolean)

    /**
     * Устанавливает callback для создания ViewBinding'a
     * */
    fun viewBinding(onCreateBinding: (parent: ViewGroup) -> VB)

    /**
     * Устанавливает callback-extension над ViewBinding'ом.
     * Вызывается перед bindViewHolder в функции ViewBindingHolder.onBindViewHolder
     *
     * @see ViewBindingHolder
     * */
    fun bindHolder(callback: VB.(value: S, position: Int) -> Unit)

    /**
     * Устанавливает callback-extension над уже созданным ViewBindingHolder'ом.
     *
     * @see ViewBindingHolder
     * */
    fun bindViewHolder(callback: ViewBindingHolder<S, VB>.(value: S, position: Int) -> Unit)
}

@DslHolder
class HolderImpl<T, S : T, VB : ViewBinding>(
    private val scope: CoroutineScope?
) : Holder<T>, HolderConfiguration<T, S, VB> {

    private var onCreateBinding: ((parent: ViewGroup) -> VB)? = null

    private var onBindHolder: (VB.(value: S, position: Int) -> Unit) = { _, _ -> }
    private var onBindViewHolder: (ViewBindingHolder<S, VB>.(value: S, position: Int) -> Unit) =
        { _, _ -> }

    private var isForMeCallback: (value: T) -> Boolean = { true }

    override fun isForMe(value: T) = isForMeCallback(value)

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val binding = onCreateBinding?.invoke(parent)
            ?: throw IllegalArgumentException("not found onCreateBinding")
        return ViewBindingHolder<S, VB>(binding, scope)
            .apply {
                setBindHolderCallback(onBindHolder)
                setBindViewHolderCallback(onBindViewHolder)
            }
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        dragHelper: DragHelper<T>,
        swipeHelper: SwipeHelper<T>,
        value: T,
        position: Int
    ) {
        @Suppress("UNCHECKED_CAST")
        ((holder as? ViewBindingHolder<T, VB>)?.onBindViewHolder(
            dragHelper,
            swipeHelper,
            value,
            position
        ))
    }

    override fun viewBinding(onCreateBinding: (parent: ViewGroup) -> VB) {
        this.onCreateBinding = onCreateBinding
    }

    override fun bindHolder(callback: VB.(value: S, position: Int) -> Unit) {
        this.onBindHolder = callback
    }

    override fun bindViewHolder(
        callback: ViewBindingHolder<S, VB>.(value: S, position: Int) -> Unit
    ) {
        this.onBindViewHolder = callback
    }

    override fun isForMe(callback: (value: T) -> Boolean) {
        this.isForMeCallback = callback
    }
}