package com.adapterdsl

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.adapterdsl.itemtouch.DragHelper
import com.adapterdsl.itemtouch.SwipeHelper
import kotlinx.coroutines.CoroutineScope

val ViewBinding.context: Context
    get() = root.context

/**
 * ViewHolder с ViewBinding. Напрямую использование запрещено.
 * Используется в [com.adapterdsl.holdersmanager.Holder].
 * */
class ViewBindingHolder<T, VB : ViewBinding>(
    val binding: VB,
    val scope: CoroutineScope?
) : RecyclerView.ViewHolder(binding.root) {

    private var onBindHolder: (VB.(value: T, position: Int) -> Unit)? = null
    private var onBindViewHolder: (ViewBindingHolder<T, VB>.(value: T, position: Int) -> Unit)? =
        null

    lateinit var dragHelper: DragHelper<T>
    lateinit var swipeHelper: SwipeHelper<T>

    fun onBindViewHolder(
        dragHelper: DragHelper<T>,
        swipeHelper: SwipeHelper<T>,
        value: T,
        position: Int
    ) {
        this.dragHelper = dragHelper
        this.swipeHelper = swipeHelper
        onBindHolder?.let { binding.it(value, position) }
        onBindViewHolder?.let { it(value, position) }
    }

    fun setBindHolderCallback(block: VB.(value: T, position: Int) -> Unit) {
        onBindHolder = block
    }

    fun setBindViewHolderCallback(
        block: ViewBindingHolder<T, VB>.(value: T, position: Int) -> Unit
    ) {
        onBindViewHolder = block
    }
}