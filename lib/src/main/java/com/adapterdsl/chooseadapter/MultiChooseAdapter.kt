package com.adapterdsl.chooseadapter

import androidx.core.view.isVisible
import com.adapterdsl.Adapter
import com.adapterdsl.databinding.ItemMultiChooseBinding
import com.adapterdsl.itemtouch.DragConfiguration
import com.adapterdsl.itemtouch.SwipeConfiguration
import com.adapterdsl.singleTypeHolder

/**
 * dsl-builder MultiChooseAdapter'a
 *
 * @param startItems стартовые значения списка. По умолчанию - пустой.
 * @param configuration ChooseAdapterConfiguration списка
 * */
fun <T> multiChooseAdapter(
    startItems: List<T> = emptyList(),
    configuration: ChooseAdapterConfiguration<T>.() -> Unit
): MultiChooseAdapter<T> {
    val adapter = MultiChooseAdapter<T>()
    val config = MultiChooseConfiguration(adapter).apply(configuration)
    return adapter.apply {
        items = startItems.toMutableList()
        singleTypeHolder<T, ItemMultiChooseBinding> {
            viewBinding { parent -> ItemMultiChooseBinding.inflate(parent.inflater, parent, false) }
            bindHolder { value, position ->
                tvTitle.text =
                    config.titleFactory?.invoke(value)
                        ?: throw IllegalArgumentException("not found title factory")

                tvSubtitle.text = config.subTitleFactory?.invoke(value) ?: ""
                tvSubtitle.isVisible = config.subTitleFactory != null

                bSwitch.isChecked = position in config.selectedIndexes

                root.setOnClickListener { config.onClick.invoke(value, position) }
            }
        }
    }
}

/**
 * Adapter для множественного выбора
 * */
class MultiChooseAdapter<T> : Adapter<T>() {

    /**
     * Индексы выбранных элементов
     * */
    var selectedIndexes = mutableListOf<Int>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun dragEndDrop(enabled: Boolean) {
        throw Exception("drag end drop недоступно для MultiChooseAdapter")
    }

    override fun dragEndDrop(enabled: Boolean, config: DragConfiguration<T>.() -> Unit) {
        throw Exception("drag end drop недоступно для MultiChooseAdapter")
    }

    override fun swipeToDismiss(enabled: Boolean) {
        throw Exception("swipe to dismiss недоступно для MultiChooseAdapter")
    }

    override fun swipeToDismiss(enabled: Boolean, config: SwipeConfiguration<T>.() -> Unit) {
        throw Exception("swipe to dismiss недоступно для MultiChooseAdapter")
    }
}

/**
 * Интерфейс для dsl-конфигурирования MultiChooseAdapter'a
 *
 * @see ChooseAdapterConfiguration
 * */
private class MultiChooseConfiguration<T>(
    private val adapter: MultiChooseAdapter<T>
) : ChooseAdapterConfiguration<T> {

    var titleFactory: ((value: T) -> String)? = null
    var subTitleFactory: ((value: T) -> String)? = null
    var onClick: ItemClick<T> = wrapClick { _, _ -> }

    /**
     * Индексы выбранных элементов
     * */
    val selectedIndexes: MutableList<Int>
        get() = adapter.selectedIndexes

    /**
     * Устанавливает фабрику title пункта.
     * Вызов этой функции обязателен, иначе будет брошено исключение IllegalArgumentException.
     * */
    override fun title(factory: (value: T) -> String) {
        titleFactory = factory
    }

    /**
     * Устанавливает фабрику subtitle пункта.
     * Вызов этой функции необязателен. В таком случае subtitle не будет отображен.
     * */
    override fun subtitle(factory: (value: T) -> String) {
        subTitleFactory = factory
    }

    /**
     * Устанавливает фабрику клика по пункту.
     * */
    override fun onClick(click: ItemClick<T>) {
        onClick = wrapClick(click)
    }

    private fun wrapClick(click: ItemClick<T>): ItemClick<T> = { value: T, position: Int ->
        val selectedIndexes = adapter.selectedIndexes
        if (position in selectedIndexes) {
            selectedIndexes -= position
        } else {
            selectedIndexes += position
        }
        adapter.notifyItemChanged(position)
        click(value, position)
    }
}