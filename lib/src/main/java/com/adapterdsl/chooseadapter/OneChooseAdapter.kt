package com.adapterdsl.chooseadapter

import androidx.core.view.isVisible
import com.adapterdsl.Adapter
import com.adapterdsl.databinding.ItemOneChooseBinding
import com.adapterdsl.itemtouch.DragConfiguration
import com.adapterdsl.itemtouch.SwipeConfiguration
import com.adapterdsl.singleTypeHolder

/**
 * dsl-builder OneChooseAdapter'a
 *
 * @param startItems стартовые значения списка. По умолчанию - пустой.
 * @param configuration ChooseAdapterConfiguration списка
 * */
fun <T> oneChooseAdapter(
    startItems: List<T> = emptyList(),
    configuration: ChooseAdapterConfiguration<T>.() -> Unit
): OneChooseAdapter<T> {
    val adapter = OneChooseAdapter<T>()
    val config = OneChooseConfiguration(adapter).apply(configuration)
    return adapter.apply {
        items = startItems.toMutableList()
        singleTypeHolder<T, ItemOneChooseBinding> {
            viewBinding { parent -> ItemOneChooseBinding.inflate(parent.inflater, parent, false) }
            bindHolder { value, position ->
                tvTitle.text =
                    config.titleFactory?.invoke(value)
                        ?: throw IllegalArgumentException("not found title factory")

                tvSubtitle.text = config.subTitleFactory?.invoke(value) ?: ""
                tvSubtitle.isVisible = config.subTitleFactory != null

                bRadio.isChecked = config.lastSelected == position

                root.setOnClickListener { config.onClick.invoke(value, position) }
            }
        }
    }
}

/**
 * Adapter для одиночного выбора
 * */
class OneChooseAdapter<T> : Adapter<T>() {

    /**
     * Индекс выбранного элемента
     * */
    var selectedIndex: Int = 0
        set(value) {
            val lastIndex = field
            field = value
            if (lastIndex != value) {
                notifyItemChanged(lastIndex)
                notifyItemChanged(value)
            }
        }

    override fun dragEndDrop(enabled: Boolean) {
        throw Exception("drag end drop недоступно для OneChooseAdapter")
    }

    override fun dragEndDrop(enabled: Boolean, config: DragConfiguration<T>.() -> Unit) {
        throw Exception("drag end drop недоступно для OneChooseAdapter")
    }

    override fun swipeToDismiss(enabled: Boolean) {
        throw Exception("swipe to dismiss недоступно для OneChooseAdapter")
    }

    override fun swipeToDismiss(enabled: Boolean, config: SwipeConfiguration<T>.() -> Unit) {
        throw Exception("swipe to dismiss недоступно для OneChooseAdapter")
    }
}

/**
 * Интерфейс для dsl-конфигурирования OneChooseAdapter'a
 *
 * @see ChooseAdapterConfiguration
 * */
private class OneChooseConfiguration<T>(
    private val adapter: OneChooseAdapter<T>
) : ChooseAdapterConfiguration<T> {

    var titleFactory: ((value: T) -> String)? = null
    var subTitleFactory: ((value: T) -> String)? = null
    var onClick: ItemClick<T> = wrapClick { _, _ -> }

    /**
     * Индекс выбранного элемента
     * */
    var lastSelected: Int
        get() = adapter.selectedIndex
        set(value) {
            adapter.selectedIndex = value
        }

    /**
     * Устанавливает фабрику title пункта.
     * Вызов этой функции обязателен, иначе будет брошено исключение IllegalArgumentException.
     * */
    override fun title(factory: (value: T) -> String) {
        titleFactory = factory
    }

    /**
     * Устанавливает фабрику subtitle пункта.
     * Вызов этой функции необязателен. В таком случае subtitle не будет отображен.
     * */
    override fun subtitle(factory: (value: T) -> String) {
        subTitleFactory = factory
    }

    /**
     * Устанавливает фабрику клика по пункту.
     * Особенностью является, то что клик на уже выбранном пункте будет игнорирован.
     * */
    override fun onClick(click: ItemClick<T>) {
        onClick = wrapClick(click)
    }

    private fun wrapClick(click: ItemClick<T>): ItemClick<T> = { value: T, position: Int ->
        if (lastSelected != position) {
            lastSelected = position
            click(value, position)
        }
    }
}