package com.adapterdsl.chooseadapter

/**
 * Интерфейс для dsl-конфигурирования ChooseAdapter'a
 * */
interface ChooseAdapterConfiguration<T> {
    /**
     * Устанавливает фабрику title пункта.
     * Вызов этой функции обязателен, иначе будет брошено исключение IllegalArgumentException.
     * */
    fun title(factory: (value: T) -> String)

    /**
     * Устанавливает фабрику subtitle пункта.
     * Вызов этой функции необязателен. В таком случае subtitle не будет отображен.
     * */
    fun subtitle(factory: (value: T) -> String)

    /**
     * Устанавливает фабрику клика по пункту.
     * */
    fun onClick(click: ItemClick<T>)
}

/**
 * Синоним клика по пункту
 * */
typealias ItemClick<T> = (value: T, position: Int) -> Unit