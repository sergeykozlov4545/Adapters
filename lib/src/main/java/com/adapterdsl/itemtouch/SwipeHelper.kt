package com.adapterdsl.itemtouch

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import com.adapterdsl.Adapter
import com.adapterdsl.DslSwipeHelper

/**
 * dsl-конфигурация смахивания пунктов списка
 * */
interface SwipeConfiguration<T> {
    /**
     * Установить callback когда начал смахиваться пункт
     * */
    fun itemStartSwipe(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit)

    /**
     * Установить callback когда был убран пункт из списка
     * */
    fun itemDismiss(callback: (adapter: Adapter<T>, position: Int) -> Unit)

    /**
     * Установить callback когда закончил смахиваться пункт
     * */
    fun itemEndSwipe(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit)
}

/**
 * Helper для смахивания пунктов списка
 * */
@DslSwipeHelper
class SwipeHelper<T>(private val adapter: Adapter<T>) : SwipeConfiguration<T> {

    private var itemStartSwipeCallback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { holder, _ -> holder.itemView.setBackgroundColor(Color.LTGRAY) }

    private var itemDismissCallback: (adapter: Adapter<T>, position: Int) -> Unit =
        { adapter, position ->
            adapter.items.removeAt(position)
            adapter.notifyItemRemoved(position)
        }

    private var itemEndSwipeCallback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { holder, _ -> holder.itemView.setBackgroundColor(0) }

    override fun itemStartSwipe(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        itemStartSwipeCallback = callback
    }

    override fun itemDismiss(callback: (adapter: Adapter<T>, position: Int) -> Unit) {
        itemDismissCallback = callback
    }

    override fun itemEndSwipe(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        itemEndSwipeCallback = callback
    }

    /**
     * Достпуно ли сейчас смахивание
     * */
    var isSwipeEnabled: Boolean = false

    /**
     * Начать принудительно смахивать пункт
     * */
    fun startSwipe(viewHolder: RecyclerView.ViewHolder) {
        adapter.startSwipe(viewHolder)
    }

    /**
     * Смахивание пункта началось
     * */
    fun onItemStartSwipe(viewHolder: RecyclerView.ViewHolder, position: Int) {
        itemStartSwipeCallback(viewHolder, position)
    }

    /**
     * Пункт убрали из списка
     * */
    fun onItemDismiss(position: Int) {
        itemDismissCallback(adapter, position)
    }

    /**
     * Смахивание пункта закончилось. Вызывается после onItemDismiss
     * */
    fun onItemEndSwipe(viewHolder: RecyclerView.ViewHolder, position: Int) {
        itemEndSwipeCallback(viewHolder, position)
    }
}