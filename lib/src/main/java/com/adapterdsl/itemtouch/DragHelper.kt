package com.adapterdsl.itemtouch

import androidx.recyclerview.widget.RecyclerView
import com.adapterdsl.Adapter
import com.adapterdsl.DslDragHelper
import java.util.*

/**
 * dsl-конфигурация перемещения пунктов списка
 * */
interface DragConfiguration<T> {
    /**
     * Установить callback когда выбран пункт для перетаскивания
     * */
    fun itemDrag(callback: (adapter: Adapter<T>, viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit)

    /**
     * Установить callback когда пункт перемещен
     * */
    fun itemMove(callback: (adapter: Adapter<T>, from: Int, to: Int) -> Boolean)

    /**
     * Установить callback когда перетаскивание пункта завершено
     * */
    fun itemDrop(callback: (adapter: Adapter<T>, viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit)
}

/**
 * Helper для перемещения пунктов списка
 * */
@DslDragHelper
class DragHelper<T>(private val adapter: Adapter<T>) : DragConfiguration<T> {

    private var itemDragCallback: (adapter: Adapter<T>, viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { _, _, _ -> }

    private var itemMoveCallback: (adapter: Adapter<T>, from: Int, to: Int) -> Boolean =
        { adapter, from, to ->
            Collections.swap(adapter.items, from, to)
            adapter.notifyItemMoved(from, to)
            true
        }

    private var itemDropCallback: (adapter: Adapter<T>, viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { _, _, _ -> }

    private var dragging: Boolean = false

    /**
     * Доступно ли перемещение пунктов
     * */
    var isDragEnabled: Boolean = false

    override fun itemDrag(callback: (adapter: Adapter<T>, viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        this.itemDragCallback = callback
    }

    override fun itemMove(callback: (adapter: Adapter<T>, from: Int, to: Int) -> Boolean) {
        this.itemMoveCallback = callback
    }

    override fun itemDrop(callback: (adapter: Adapter<T>, viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        this.itemDropCallback = callback
    }

    /**
     * Перемещается ли сейчас какой-то пункт списка
     * */
    fun isDragging(): Boolean = dragging

    /**
     * Начать принудительно перетаскивать пункт
     * */
    fun startDrag(viewHolder: RecyclerView.ViewHolder) {
        adapter.startDrag(viewHolder)
    }

    /**
     * Пункт выбран для перетаскивания
     * */
    fun onItemDrag(viewHolder: RecyclerView.ViewHolder, position: Int) {
        dragging = true
        itemDragCallback(adapter, viewHolder, position)
    }

    /**
     * Пункт перемещен, но еще выбран для дальнейшего перетаскивания
     * */
    fun onItemMove(from: Int, to: Int): Boolean {
        return itemMoveCallback(adapter, from, to)
    }

    /**
     * Перемещение пункта завершено
     * */
    fun onItemDrop(viewHolder: RecyclerView.ViewHolder, position: Int) {
        dragging = false
        itemDropCallback(adapter, viewHolder, position)
    }
}