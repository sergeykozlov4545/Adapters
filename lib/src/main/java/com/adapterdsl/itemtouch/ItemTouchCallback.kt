package com.adapterdsl.itemtouch

import android.graphics.Canvas
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs

/**
 * ItemTouchHelper.Callback для управления ItemTouchHelper
 *
 * @see ItemTouchHelper
 * */
class ItemTouchCallback<T>(
    private val dragHelper: DragHelper<T>,
    private val swipeHelper: SwipeHelper<T>
) : ItemTouchHelper.Callback() {

    private var selectedActionState: Int = ItemTouchHelper.ACTION_STATE_IDLE

    override fun isLongPressDragEnabled(): Boolean = dragHelper.isDragEnabled

    override fun isItemViewSwipeEnabled(): Boolean = swipeHelper.isSwipeEnabled

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val isGrid = recyclerView.layoutManager is GridLayoutManager
        var dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        if (isGrid) {
            dragFlags = dragFlags or ItemTouchHelper.START or ItemTouchHelper.END
        }
        val swipeFlags = if (isGrid) 0 else ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        val from = viewHolder.adapterPosition
        val to = target.adapterPosition
        val canMove = viewHolder.itemViewType == target.itemViewType && from != to
        return if (canMove) dragHelper.onItemMove(from, to) else false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        swipeHelper.onItemDismiss(viewHolder.adapterPosition)
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)
        val position = viewHolder?.adapterPosition ?: RecyclerView.NO_POSITION
        if (position != RecyclerView.NO_POSITION) {
            selectedActionState = actionState
            when (actionState) {
                ItemTouchHelper.ACTION_STATE_DRAG -> {
                    dragHelper.onItemDrag(viewHolder!!, position)
                }
                ItemTouchHelper.ACTION_STATE_SWIPE -> {
                    swipeHelper.onItemStartSwipe(viewHolder!!, position)
                }
            }
        }
    }

    override fun clearView(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ) {
        super.clearView(recyclerView, viewHolder)
        viewHolder.itemView.alpha = ALPHA_FULL
        val position = viewHolder.adapterPosition
        when (selectedActionState) {
            ItemTouchHelper.ACTION_STATE_DRAG -> {
                dragHelper.onItemDrop(viewHolder, position)
            }
            ItemTouchHelper.ACTION_STATE_SWIPE -> {
                swipeHelper.onItemEndSwipe(viewHolder, position)
            }
        }
        selectedActionState = ItemTouchHelper.ACTION_STATE_IDLE
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState != ItemTouchHelper.ACTION_STATE_SWIPE) {
            super.onChildDraw(
                c,
                recyclerView,
                viewHolder,
                dX,
                dY,
                actionState,
                isCurrentlyActive
            )
            return
        }
        viewHolder.itemView.alpha = ALPHA_FULL - abs(dX) / viewHolder.itemView.width
        viewHolder.itemView.translationX = dX
    }

    private companion object {
        private const val ALPHA_FULL = 1f
    }
}