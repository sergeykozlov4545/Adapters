package com.adapterdsl

import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.adapterdsl.diffutil.DiffCallback
import com.adapterdsl.diffutil.DiffCallbackConfiguration
import com.adapterdsl.holdersmanager.Holder
import com.adapterdsl.holdersmanager.HolderConfiguration
import com.adapterdsl.holdersmanager.HolderImpl
import com.adapterdsl.holdersmanager.HoldersManager
import com.adapterdsl.itemtouch.*
import kotlinx.coroutines.CoroutineScope

/**
 * dsl-builder Adapter'a
 *
 * @see Adapter
 * */
fun <T> adapter(
    startItems: List<T> = emptyList(),
    configure: Adapter<T>.() -> Unit
) =
    Adapter<T>().apply { items = startItems.toMutableList() }.apply(configure)

/**
 * dsl-builder Holder'a
 *
 * @see Holder
 * @see HolderConfiguration
 * */
inline fun <T, reified S : T, VB : ViewBinding> Adapter<T>.holder(
    scope: CoroutineScope? = null,
    configure: HolderConfiguration<T, S, VB>.() -> Unit
) {
    val holder = HolderImpl<T, S, VB>(scope)
    holder.isForMe { value -> value is S }
    addHolder(holder.apply(configure))
}

/**
 * dsl-builder Holder'a у которого Т (родитель) совпадает с S (наследник)
 *
 * @see Holder
 * @see HolderConfiguration
 * */
inline fun <T, VB : ViewBinding> Adapter<T>.singleTypeHolder(
    scope: CoroutineScope? = null,
    configure: HolderConfiguration<T, T, VB>.() -> Unit
) {
    val holder = HolderImpl<T, T, VB>(scope)
    addHolder(holder.apply(configure))
}

/**
 * RecyclerView.Adapter для отображения списков
 * */
@DslAdapter
open class Adapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val holdersManager = HoldersManager<T>()

    private var diffCalculator: AsyncListDiffer<T>? = null

    private var dragHelper: DragHelper<T> = DragHelper(this)
    private var swipeHelper: SwipeHelper<T> = SwipeHelper(this)

    private var bindViewHolderCallback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { _, _ -> }

    private var attachedHolderCallback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { _, _ -> }

    private var detachedHolderCallback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit =
        { _, _ -> }

    private val touchHelper by lazy {
        ItemTouchHelper(ItemTouchCallback(dragHelper, swipeHelper))
    }

    /**
     * Список пунктов
     * */
    var items: MutableList<T> = mutableListOf()
        set(value) {
            field = value
            diffCalculator?.submitList(value) ?: notifyDataSetChanged()
        }

    /**
     * Доступно ли перетаскивание пунктов при долгом нажатии
     * */
    var isDragEnabled: Boolean = dragHelper.isDragEnabled
        set(value) {
            field = value
            dragHelper.isDragEnabled = value
        }

    /**
     * Доступно ли смахивание пунктов
     * */
    var isSwipeEnabled: Boolean = swipeHelper.isSwipeEnabled
        set(value) {
            field = value
            swipeHelper.isSwipeEnabled = value
        }

    /**
     * Начать принудительно перетаскивать пункт
     * */
    fun startDrag(viewHolder: RecyclerView.ViewHolder) {
        touchHelper.startDrag(viewHolder)
    }

    /**
     * Начать принудительно смахивать пункт
     * */
    fun startSwipe(viewHolder: RecyclerView.ViewHolder) {
        touchHelper.startSwipe(viewHolder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        holdersManager.onCreateViewHolder(parent, viewType)

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) =
        holdersManager.getItemViewType(items, position)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        bindViewHolderCallback(holder, position)
        holdersManager.onBindViewHolder(holder, dragHelper, swipeHelper, items, position)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        touchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        touchHelper.attachToRecyclerView(null)
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        attachedHolderCallback(holder, holder.adapterPosition)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        detachedHolderCallback(holder, holder.adapterPosition)
    }

    /**
     * Установить конфигурацию DiffUtil
     *
     * @see DiffCallbackConfiguration
     * */
    fun diffCallback(configuration: DiffCallbackConfiguration<T>.() -> Unit) {
        diffCalculator = DiffCallback<T>().apply(configuration).createDiffer(this)
    }

    /**
     * Установить onBindViewHolder callback
     * */
    fun bindViewHolder(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        this.bindViewHolderCallback = callback
    }

    /**
     * Установить callback когда вызовется [RecyclerView.Adapter.onViewAttachedToWindow]
     * */
    fun attachedHolder(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        this.attachedHolderCallback = callback
    }

    /**
     * Установить callback когда вызовется [RecyclerView.Adapter.onViewDetachedFromWindow]
     * */
    fun detachedHolder(callback: (viewHolder: RecyclerView.ViewHolder, position: Int) -> Unit) {
        this.detachedHolderCallback = callback
    }

    /**
     * Добавить Holder к Adapter'у.
     * Неиспользовать его напрямую. Для этого есть dsl-builder holder/singleTypeHolder
     *
     * @see Holder
     * @see Adapter.holder
     * @see Adapter.singleTypeHolder
     * */
    fun addHolder(holder: Holder<T>) {
        holdersManager += holder
    }

    /**
     * Builder для DragHelper с конфигурацией по умолчанию
     *
     * @see DragHelper
     * */
    open fun dragEndDrop(enabled: Boolean) {
        dragHelper = DragHelper(this)
            .apply { isDragEnabled = enabled }
    }

    /**
     * Builder для DragHelper с кастомной конфигурацией
     *
     * @see DragHelper
     * @see DragConfiguration
     * */
    open fun dragEndDrop(enabled: Boolean, config: DragConfiguration<T>.() -> Unit) {
        dragHelper = DragHelper(this)
            .apply { isDragEnabled = enabled }
            .apply(config)
    }

    /**
     * Builder для SwipeHelper с конфигурацией по умолчанию
     *
     * @see SwipeHelper
     * */
    open fun swipeToDismiss(enabled: Boolean) {
        swipeHelper = SwipeHelper(this)
            .apply { isSwipeEnabled = enabled }
    }

    /**
     * Builder для SwipeHelper с кастомной конфигурацией
     *
     * @see SwipeHelper
     * @see SwipeConfiguration
     * */
    open fun swipeToDismiss(enabled: Boolean, config: SwipeConfiguration<T>.() -> Unit) {
        swipeHelper = SwipeHelper(this)
            .apply { isSwipeEnabled = enabled }
            .apply(config)
    }
}